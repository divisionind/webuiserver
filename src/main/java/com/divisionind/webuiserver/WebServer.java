/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.divisionind.webuiserver;

import com.google.common.collect.Ordering;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Comparator;

public class WebServer {

    private HttpServer server;
    
    public WebServer(int port) throws IOException {
        this.server = HttpServer.create(new InetSocketAddress(port), 0);
        String photosPath = Run.PHOTOS.getAbsolutePath();
        // creates context handlers (e.g. example.com/, example.com/a, example.com/image
        StaticFileHandler.create(this.server, "/", Run.HTML.getAbsolutePath(), "index.html");
        StaticFileHandler.create(this.server, "/a/", photosPath, "index.html");      // for base image data transfer
        this.server.createContext("/image", new ImageHandler("/image", photosPath)); // for modified image view
        this.server.setExecutor(null); // creates a default executor
    }
    
    public void start() {
        server.start();
    }
    
    private static class ImageHandler implements HttpHandler {

        private String urlPrefix;
        private String rootPath;
        private Comparator<File> fileSorter;
        
        public ImageHandler(String urlPrefix, String path) throws IOException {
            this.urlPrefix = urlPrefix;
            this.rootPath = new File(path).getCanonicalPath();
            this.fileSorter = Ordering.from(new FileTypeComparator()).compound(new FileNameComparator());
        }
        
        @Override
        public void handle(HttpExchange t) throws IOException {
            String wholeUrlPath = t.getRequestURI().getPath();
            if (!wholeUrlPath.startsWith(urlPrefix)) throw new RuntimeException("Path is not in prefix - incorrect routing?");
            String urlPath = wholeUrlPath.substring(urlPrefix.length());
            File f = new File(rootPath, urlPath);
            Run.log(f.getAbsolutePath());
            int dots = countNumberOfChars(urlPath, '/') + 1; // numSlashes + 1 = num of .. to have
            StringBuilder sbd = new StringBuilder();
            for (int i = 0;i<dots;i++) sbd.append("../");
            String rebound = sbd.toString();

            DoubleVar<String, String> replace = new DoubleVar<>("%ELEMENTS%", null);
            if (f.exists()) {
                if (f.isDirectory()) {
                    File[] files = f.listFiles();
                    if (files.length == 0) replace.setVar2("Directory is empty"); else {
                        StringBuilder sb = new StringBuilder();
                        ByteArrayOutputStream elementBAO;
                        DoubleVar<String, String> image_src = new DoubleVar<>("%IMAGE_SRC%", null);
                        DoubleVar<String, String> image_name = new DoubleVar<>("%IMAGE_NAME%", null);
                        DoubleVar<String, String> image_target = new DoubleVar<>("%IMAGE_TARGET%", null);

                        // sorts all files so they show like a more typical file explorer
                        Arrays.sort(files, fileSorter);
                        for (File fi : files) {
                            String name = fi.getName();
                            image_name.setVar2(name); // remove file extension?
                            String pre;
                            if (fi.isDirectory()) {
                                pre = "/image";
                                image_src.setVar2(rebound + "folder.png"); // you can also use Run.getBindAddress() here
                            } else {
                                if (!(name.endsWith(".jpg") || name.endsWith(".png"))) continue;
                                pre = "/a"; // replace with custom template?
                                image_src.setVar2(rebound + "a" + urlPath + "/" + name);
                            }
                            image_target.setVar2(pre + urlPath + "/" + name);
                            elementBAO = readTemplate("image_browse_element.html", image_src, image_name, image_target);
                            sb.append(elementBAO.toString()).append("\n");
                        }
                        replace.setVar2(sb.toString());
                    }
                } else replace.setVar2("The path given is not a directory");
            } else replace.setVar2("Object at this path does not exist");
            ByteArrayOutputStream data = readTemplate("image_browse_main.html", replace);
            writeResponse(t, data.toByteArray());
        }
    }

    // not the best way to do this but ehh, it works
    public static ByteArrayOutputStream readTemplate(String template, DoubleVar<String, String>... replaces) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(bao));
        File templateFile = new File(Run.TEMPLATES, template);
        if (templateFile.exists()) {
            BufferedReader br = new BufferedReader(new FileReader(templateFile));
            br.lines().forEachOrdered(s -> {
                for (DoubleVar<String, String> repl : replaces) {
                    if (s.contains(repl.getVar1())) s = s.replaceAll(repl.getVar1(), repl.getVar2());
                }
                pw.println(s);
            });
            br.close();
        } else pw.write("Could not locate template file: " + template);
        pw.flush();
        pw.close();
        return bao;
    }

    public static int countNumberOfChars(String s, char c) {
        int i = 0;
        for (char ci : s.toCharArray()) {
            if (c == ci) i++;
        }
        return i;
    }
    
    public static void writeResponse(HttpExchange t, byte[] response) throws IOException {
        //byte[] response = resp.getBytes();
        t.sendResponseHeaders(200, response.length);
        OutputStream os = t.getResponseBody();
        os.write(response);
        os.flush();
        os.close();
    }

    private static class FileTypeComparator implements Comparator<File> {
        @Override
        public int compare(File file1, File file2) {
            if (file1.isDirectory() && file2.isFile()) return -1;
            if (file1.isDirectory() && file2.isDirectory()) return 0;
            if (file1.isFile() && file2.isFile()) return 0;
            return 1;
        }
    }

    private static class FileNameComparator implements Comparator<File> {
        @Override
        public int compare(File file1, File file2) {
            return String.CASE_INSENSITIVE_ORDER.compare(file1.getName(), file2.getName());
        }
    }
}
