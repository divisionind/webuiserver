/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.divisionind.webuiserver;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DateFormat;
import java.util.Date;

public class Run {

    public static final File ROOT = new File("").getAbsoluteFile();
    public static final File HTML = new File(ROOT, "html");
    public static final File TEMPLATES = new File(ROOT, "templates");

    public static File PHOTOS = new File("E:\\Photos");
    public static File THUMBNAILS_INDEX = new File(ROOT, "_thumbnails_");
    private static String BIND_ADDRESS = "localhost"; // not really functionally required (idk why I added it)
    private static int PORT = 80;
    private static int MAX_THUMBNAIL_WIDTH = 300;

    private static final Date date = new Date();
    private static TrayIcon tray;
    private static WebServer server;
    public static ThumbnailManager thumb;

    public static void main(String[] args) { // https://www.w3schools.com/css/css_image_gallery.asp
        System.out.println("Initializing...");
        File configFile = new File(ROOT, "config.properties");
        PropertiesConfiguration c = new PropertiesConfiguration();
        try {
            if (configFile.exists()) {
                // load
                c.read(new FileReader(configFile));
                PORT = c.getInt("port", PORT);
                PHOTOS = c.get(File.class, "photos", PHOTOS);
                THUMBNAILS_INDEX = c.get(File.class, "thumbnail_store", THUMBNAILS_INDEX);
                MAX_THUMBNAIL_WIDTH = c.getInt("max_thumbnail_width", MAX_THUMBNAIL_WIDTH);
                BIND_ADDRESS = c.getString("bind_address", BIND_ADDRESS);
            } else {
                // create
                log("Configuration did not exist. Creating one at: " + configFile.getAbsolutePath());
                c.setHeader("WebUIServer Configuration File");
                c.addProperty("port", PORT);
                c.addProperty("photos", PHOTOS);
                c.addProperty("thumbnail_store", THUMBNAILS_INDEX);
                c.addProperty("max_thumbnail_width", MAX_THUMBNAIL_WIDTH);
                c.addProperty("bind_address", BIND_ADDRESS);
                c.write(new FileWriter(configFile));
            }
        } catch (IOException | ConfigurationException e) {
            log("Error loading configuration: " + e.getLocalizedMessage(), System.err);
            e.printStackTrace();
        }

        if (!PHOTOS.exists()) {
            log("Error: Could not find photos directory (does not exist).", System.err);
            return;
        }

        if (!HTML.exists()) HTML.mkdirs();
        if (!TEMPLATES.exists()) TEMPLATES.mkdirs();
        if (!THUMBNAILS_INDEX.exists()) THUMBNAILS_INDEX.mkdirs();

        // check/create tray icon
        if (SystemTray.isSupported()) {
            try {
                tray = new TrayIcon(resize(ImageIO.read(Run.class.getResourceAsStream("/assets/server-icon.png")), 16), "WebUIServer", new TrayUI());
                SystemTray.getSystemTray().add(tray);
                log("Added system tray icon!");
            } catch (IOException | AWTException e) { }
        }

        log("Starting thumbnail generator daemon...");
        thumb = new ThumbnailManager(THUMBNAILS_INDEX, PHOTOS, MAX_THUMBNAIL_WIDTH);
        log(" > Started.");

        // start webserver
        log("Started webserver at " + getBindAddress());
        try {
            server = new WebServer(PORT);
            server.start();
        } catch (IOException e) {
            log("Error starting webserver: " + e.getLocalizedMessage(), System.err);
            e.printStackTrace();
        }
    }

    public static void log(Object msg) {
        log(msg, System.out);
    }

    public static void log(Object msg, PrintStream out) {
        date.setTime(System.currentTimeMillis());
        out.print("[");
        out.print(DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.DEFAULT).format(date));
        out.print("] ");
        out.print(msg);
        out.println();
    }

    public static String getBindAddress() {
        return "http://" + BIND_ADDRESS + (PORT == 80 ? "" : (":" + PORT)) + "/";
    }

    public static Image resize(BufferedImage source, int new_h) {
        double w = new_h / (double)source.getHeight() * (double)source.getWidth();
        return source.getScaledInstance((int)w, new_h, Image.SCALE_AREA_AVERAGING);
    }
}
