/*
 * Copyright 2017-2018 Division Industries LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.divisionind.webuiserver;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ThumbnailManager {

    private int max_width;
    private int photosPathLength;
    private String thumbRootPath;
    private File photos;
    private Thread thumbWorkerThread;
    private ThumbWorker thumbWorker;

    public ThumbnailManager(File thumbnailsIndex, File photos, int max_width) {
        this.max_width = max_width;
        this.photos = photos;
        this.photosPathLength = photos.getAbsolutePath().length();
        this.thumbRootPath = thumbnailsIndex.getAbsolutePath();
    }

    public void regenerate(Runnable fin, Runnable inc) {
        if (isRegeneratingThumbnails()) throw new IllegalStateException("Thumbnail generator already running");
        this.thumbWorkerThread = new Thread(thumbWorker = new ThumbWorker(fin, inc));
        this.thumbWorkerThread.setDaemon(true);
        this.thumbWorkerThread.setName("thumbnailGenerator");
        this.thumbWorkerThread.start();
    }

    public File getThumbnailInstance(File image) {
        // pass normal image through if no thumbnail instance available (this will make thumbnails an optional optimization only feature)
        String path = image.getAbsolutePath().substring(photosPathLength, image.getAbsolutePath().length() - 3);
        File scaled = new File(thumbRootPath + path + "png");
        if (scaled.exists()) return scaled; else return image;
    }

    public boolean isRegeneratingThumbnails() {
        return thumbWorkerThread != null;
    }

    private void forEachFile(File file, FileRunnable fr) {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) forEachFile(f, fr);
        } else fr.run(file);
    }

    public ThumbWorker getThumbWorker() {
        return thumbWorker;
    }

    public class ThumbWorker implements Runnable {

        private Runnable fin;
        private Runnable inc;
        private long processed;
        private long total;

        public ThumbWorker(Runnable fin, Runnable inc) {
            this.fin = fin;
            this.inc = inc;
            this.processed = 0L;
            this.total = 0L;
            forEachFile(photos, f -> {
                String name = f.getName();
                if (!(name.endsWith(".png") || name.endsWith(".jpg"))) return;
                total++;
            });
        }

        @Override
        public void run() {
            try {
                forEachFile(photos, f -> {
                    String name = f.getName();
                    if (!(name.endsWith(".png") || name.endsWith(".jpg"))) return;
                    String path = f.getAbsolutePath().substring(photosPathLength, f.getAbsolutePath().length() - 3);
                    File scaled = new File(thumbRootPath + path + "png");
                    File scaledParent = scaled.getParentFile();
                    if (!scaledParent.exists()) scaledParent.mkdirs();
                    System.out.println(scaled);
                    try {
                        //Image img = resize(ImageIO.read(f), max_width);
                        Image img = resizeWidth(ImageIO.read(f), max_width); // use https://libjpeg-turbo.org/ for faster img processing
                        ImageIO.write(toBufferedImage(img), "png", scaled);
                        processed++;
                        inc.run();
                    } catch (IOException e) { }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            fin.run();
            thumbWorkerThread = null;
            thumbWorker = null;
        }

        public long getProcessed() {
            return processed;
        }

        public long getTotal() {
            return total;
        }

        public double getPercentComplete() {
            return new BigDecimal(processed).divide(new BigDecimal(total), new MathContext(5, RoundingMode.HALF_UP)).doubleValue() * 100.0D;
        }

        public double round(double value, int places) {
            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }

        private Image resize(BufferedImage source, int maxw, int maxh) { // TODO compress images (along with scaling them)
            return null;
        }

        private Image resizeWidth(BufferedImage source, int width) {
            double new_h = ((double)width / (double)source.getWidth()) * (double)source.getHeight();
            return source.getScaledInstance(width, (int)new_h, Image.SCALE_AREA_AVERAGING);
        }

        private Image resizeHeight(BufferedImage source, int new_h) {
            double w = new_h / (double)source.getHeight() * (double)source.getWidth();
            return source.getScaledInstance((int)w, new_h, Image.SCALE_AREA_AVERAGING);
        }

        private BufferedImage toBufferedImage(Image img) {
            if (img instanceof BufferedImage) {
                return (BufferedImage) img;
            }

            BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            Graphics2D bGr = bimage.createGraphics();
            bGr.drawImage(img, 0, 0, null);
            bGr.dispose();

            return bimage;
        }
    }

    private interface FileRunnable {
        void run(File f);
    }
}
